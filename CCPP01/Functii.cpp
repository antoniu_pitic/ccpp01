#include "Functii.h"

int Maxim(int a, int b)
{
	return a > b ? a : b;
}

int Minim(int a, int b)
{
	return a + b - Maxim(a, b);
}

int Minim(int a, int b, int c, int d)
{
//	return (a<b) ? ( (a<c) ? ((a<d) ? a:d) ): ((c<d) ? c:d)) : ((b < c) ? ((b < d) ? b : d) : ((c < d) ? c : d) );

	return Minim(Minim(a, b), Minim(c, d));
}

int f(int a, int b, int c)
{
	return a*b - c;
}
int Minim(int a, int b);
